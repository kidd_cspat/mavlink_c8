/**
  ******************************************************************************
  * @file           user_timer.c
  * @brief          用户软件定时器库函数
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 kidd.
  * All rights reserved.
  ******************************************************************************
  */
#include "user_timer.h"

//时基累加最大值，16位的
#define TIMERBASE_MAX 0xFFFF

//时基，放入定时器中累加
volatile uint16_t g_timer_base;

/**
  * @brief  软件定时器
  * @param  clk:时间基准
  * @param  timer_switch：定期器开启条件（1为开启，0为关闭）
  * @param  timer_vlaue：定时时间（单位为时基的单位，timer_switch为0时，此项不起作用）
  * @param  P: 定时器结构体指针
  * @retval None
  */
void timerON(uint16_t clk, uint8_t timer_switch, uint16_t timer_vlaue, UserTimer_t* p)
{
  //定时器关闭，复位定时器状态
  if (!timer_switch)                                                            
  {
    p->timer_sum_value = 0;
    p->timer_timeout_flag = 0;
  }
  else
  {
    //定时器开启，定时时间未到
    if (p->timer_timeout_flag == 0)                                             
    {
      //时基未溢出
      if (clk > p->timer_last_value)                                           
      {
        //累加计数
        p->timer_sum_value += (clk - p->timer_last_value);                      
      }
      //时基溢出
      else if (clk < p->timer_last_value)                                       
      {
        //累加计数，加时基累加最大值
        p->timer_sum_value += ((TIMERBASE_MAX - p->timer_last_value) + clk);    
      }
    }
    //判断是否到时
    p->timer_timeout_flag = (p->timer_sum_value >= timer_vlaue);                
  }
  //更新上次定时器值
  p->timer_last_value = clk;                                                    
}

/**
  * @brief  软件计数器
  * @param  clk:时间基准
  * @param  timer_switch：定期器开启条件（1为开启，0为关闭）
  * @param  P: 定时器结构体指针
  * @retval 是否可以读取标记
  */
uint8_t timerSum(uint16_t clk, uint8_t timer_switch, UserTimerSum_t* p)
{
  //开启计数器
  if(timer_switch)
  {
    p->timer_start = clk;
  }
  else
  {
    if (clk > p->timer_start) 
    {
      //时基未溢出，累加计数
      p->timer_duration = clk - p->timer_start;  
    }
    else
    {
      //时基溢出，加时基累加最大值
      p->timer_duration =((TIMERBASE_MAX - p->timer_start) + clk); 
    }
    //返回可以读取计数值
    return 1;
  }
  //返回不能读取计数值
  return 0;
}










