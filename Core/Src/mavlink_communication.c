/**
  ******************************************************************************
  * @file           mavlink_communication.c
  * @brief          mavlink接收处理
  ******************************************************************************
  * @attention      1. 接收到的数据保存；2. 用户给的控制指令发送
  *
  * Copyright (c) 2023 kidd.
  * All rights reserved.
  ******************************************************************************
  */
#include "mavlink_communication.h"
#include "usart.h"
#include "mavlink.h"
#include <math.h>

uint8_t mavlink_tx_buf[MAVLINK_BUFFER_SIZE];
uint8_t mavlink_rx_buf[MAVLINK_BUFFER_SIZE];
mavlink_message_t msg_receive;
mavlink_message_t msg_send;

uint8_t g_mav_receive = 0;                                                 //0 空闲； 1 忙

mavlink_global_position_int_t g_pos;                                            //mavlink收到的gps坐标
mavlink_command_ack_t g_ack;   

extern DMA_HandleTypeDef hdma_usart1_rx;
/**
  * @brief  mavlink串口回调函数
  * @param  None
  * @retval None
  */
void mavlinkDataCallback(void)
{
  g_mav_receive = 1;
  uint8_t data_length;
  mavlink_status_t status;
  //判断如果是空闲中断
  if (RESET != __HAL_UART_GET_FLAG(&huart1, UART_FLAG_IDLE))
  {
    //第一步：清除空闲中断标志
    __HAL_UART_CLEAR_IDLEFLAG(&huart1);
    //第二步：停止DMA传输
    HAL_UART_DMAStop(&huart1);
    //第三步：计算接收到数据的长度
    data_length  = MAVLINK_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(&hdma_usart1_rx);
    //第四步：开始处理数据
    for (int i = 0; i < data_length; i++)
    {
      if (MAVLINK_FRAMING_OK == mavlink_parse_char(MAVLINK_COMM_0, mavlink_rx_buf[i], &msg_receive, &status))
      {
        switch (msg_receive.msgid)
        {
          case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:                              //拿到gps数据
            mavlink_msg_global_position_int_decode(&msg_receive, &g_pos);
            break;
          case MAVLINK_MSG_ID_COMMAND_ACK:
            mavlink_msg_command_ack_decode(&msg_receive, &g_ack);                 //拿到反馈数据
            break;
          default:
            break;
        }
      }
    }
    //第五步：缓存清零
    memset(mavlink_rx_buf, 0, MAVLINK_BUFFER_SIZE);
    //第六步：重新开启DMA传输
    HAL_UART_Receive_DMA(&huart1, mavlink_rx_buf, MAVLINK_BUFFER_SIZE);
  }
  g_mav_receive = 0;
}

/**
  * @brief  发送经纬高给无人机去到指定位置（高精度）
  * @param  _lat  经度  放大10^7发送给飞控
            _lon  纬度  放大10^7发送给飞控
            _alt  高度  单位米
  * @retval None
  */
void writeGlobalPositionInt(int32_t _lat, int32_t _lon, float _alt)           //发送目标点
{
  mavlink_command_int_t global_position_int;
  global_position_int.target_component = 1;
  global_position_int.target_system = 1;
  global_position_int.frame = MAV_FRAME_GLOBAL_RELATIVE_ALT; //Global (WGS84) coordinate frame + MSL altitude. First value / x: latitude, second value / y: longitude, third value / z: positive altitude over mean sea level (MSL).
  global_position_int.command = MAV_CMD_DO_REPOSITION;
  global_position_int.current = 0;
  global_position_int.autocontinue = 0;
  global_position_int.param1 = -1.0;     //Ground speed, less than 0 (-1) for default
  global_position_int.param2 = 1;      //Bitmask of option flags. 立即转换到导航状态
  global_position_int.param3 = 0;      //Reserved
  global_position_int.param4 = NAN;    //Yaw heading. NaN to use the current system yaw
  global_position_int.x = _lat;        //int32_t global: longitude in degrees * 10^7
  global_position_int.y = _lon;        //int32_t global: longitude in degrees * 10^7
  global_position_int.z = _alt;        //float altitude in meters (relative or absolute, depending on frame).
  
  mavlink_msg_command_int_encode(255, 1, &msg_send, &global_position_int);
  uint16_t len = mavlink_msg_to_send_buffer(mavlink_tx_buf, &msg_send);
  HAL_UART_Transmit(&huart1, mavlink_tx_buf, len, 0xFF);
}

/**
  * @brief  发送经纬高给无人机去到指定位置（低精度）
  * @param  _lat  经度  实际值，精度6位
            _lon  纬度  实际值，精度6位
            _alt  高度  单位米
  * @retval None
  */
void writeGlobalPositionLong(float _lat, float _lon, float _alt)                //发送目标点，WGS84 global position.
{
  mavlink_command_long_t global_position_long;
  global_position_long.target_component = 1;
  global_position_long.target_system = 1;
  global_position_long.command = MAV_CMD_DO_REPOSITION;
  global_position_long.confirmation = 0;
  global_position_long.param1 = -1;          //Ground speed, less than 0 (-1) for default
  global_position_long.param2 = 1;           //Bitmask of option flags. 立即转换到导航状态
  global_position_long.param3 = 0;           //Reserved
  global_position_long.param4 = 0;           //Yaw heading. NaN to use the current system yaw
  global_position_long.param5 = _lat;        //float 
  global_position_long.param6 = _lon;        //float 
  global_position_long.param7 = _alt;        //float altitude in meters (relative or absolute, depending on frame).
  
  mavlink_msg_command_long_encode(255, 1, &msg_send, &global_position_long);
  uint16_t len = mavlink_msg_to_send_buffer(mavlink_tx_buf, &msg_send);
  HAL_UART_Transmit(&huart1, mavlink_tx_buf, len, 0xFF);
}

