/**
  ******************************************************************************
  * @file           user_communication.c
  * @brief          用户接收处理
  ******************************************************************************
  * @attention      1. 接收到的数据保存；2. 发送给用户需要的数据（GPS）
  *
  * Copyright (c) 2023 kidd.
  * All rights reserved.
  ******************************************************************************
  */
#include "user_communication.h"
#include "usart.h"
#include "crc_table.h"

uint8_t control_tx_buf[USER_BUFFER_SIZE];
uint8_t control_rx_buf[USER_BUFFER_SIZE];
gps_frame_struct g_gps_frame;
gps_struct g_send_gps_data;
uint8_t g_user_receive;

extern DMA_HandleTypeDef hdma_usart2_rx;
/**
  * @brief  用户数据回调函数
  * @param  None
  * @retval None
  */
void controlDataCallback(void)
{
  uint8_t data_length;
  //判断如果是空闲中断
  if (RESET != __HAL_UART_GET_FLAG(&huart2, UART_FLAG_IDLE))
  {
    //第一步：清除空闲中断标志
    __HAL_UART_CLEAR_IDLEFLAG(&huart2);
    //第二步：停止DMA传输
    HAL_UART_DMAStop(&huart2);
    //第三步：计算接收到数据的长度
    data_length  = USER_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(&hdma_usart2_rx);
    //第四步：开始处理数据
    g_gps_frame = *(gps_frame_struct *)control_rx_buf;
    //数据校验通过
    if(g_gps_frame.header1 == 0x5A && g_gps_frame.header2 == 0xA5
       && g_gps_frame.crc16 == CRC16_Table(g_gps_frame.data.data_buff, g_gps_frame.len))
    {
      g_send_gps_data = g_gps_frame.data.data_pack;
      g_user_receive = 1;
    }
    //第五步：缓存清零
    memset(control_rx_buf, 0, USER_BUFFER_SIZE);
    //第六步：重新开启DMA传输
    HAL_UART_Receive_DMA(&huart2, control_rx_buf, USER_BUFFER_SIZE);
  }
}

/**
  * @brief  发送经纬高给用户
  * @param  _lat  经度
            _lon  纬度
            _alt  高度
  * @retval None
  */
void feedbackGPSDate(int32_t _lat, int32_t _lon, float _alt)
{
  gps_frame_struct temp_gps_frame = {0};
  temp_gps_frame.header1 = 0x5A;
  temp_gps_frame.header2 = 0xA5;
  temp_gps_frame.len = 12;
  temp_gps_frame.data.data_pack.lat = _lat;
  temp_gps_frame.data.data_pack.lon = _lon;
  temp_gps_frame.data.data_pack.alt = _alt;
  temp_gps_frame.crc16 = CRC16_Table(temp_gps_frame.data.data_buff, temp_gps_frame.len);
  
  HAL_UART_Transmit(&huart2, (uint8_t *)&temp_gps_frame, sizeof(gps_frame_struct), 0xFF);
}



