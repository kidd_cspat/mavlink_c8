/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include "mavlink_communication.h"
#include "user_communication.h"
#include "user_timer.h"
#include "lib_queue.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
UserTimer_t timer_10ms, timer_100ms, timer_1s;
QUEUE_DATA_T rx_queue_buff[4095];
QUEUE_HandleTypeDef rx_queue;                                                   //定义队列
uint8_t rx_buffer;                                                              //接收中断缓冲
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void update(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  Queue_Init(&rx_queue, rx_queue_buff, sizeof(rx_queue_buff));                  //初始化队列，开辟空间
  /* USER CODE BEGIN 2 */
  //开启串口空闲中断
//  __HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);

  __HAL_UART_ENABLE_IT(&huart2, UART_IT_IDLE);
  //开启DMA传输
//  HAL_UART_Receive_DMA(&huart1, mavlink_rx_buf, MAVLINK_BUFFER_SIZE);
  HAL_UART_Receive_IT(&huart1, (uint8_t*)&rx_buffer, 1);
  HAL_UART_Receive_DMA(&huart2, control_rx_buf, USER_BUFFER_SIZE);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    //1.打开100Hz定时器，定时检查是否有用户指令过来，如果有就发给飞控
    timerON(g_timer_base, 1, 10, &timer_10ms);
    //1.1 10ms检查一次标记位
    if (timer_10ms.timer_timeout_flag)
    {
      if (g_user_receive == 1)                                                  //如果收到一条数据就发送一条
      {
        writeGlobalPositionInt(g_gps_frame.data.data_pack.lat, g_gps_frame.data.data_pack.lon, g_gps_frame.data.data_pack.alt);
        g_user_receive = 0;
      }
      update();                                                                 //更新接收数据
      timerON(g_timer_base, 0, 10, &timer_10ms);
    }

    //2. 打开10Hz定时器,定时发送gps数据给用户
    timerON(g_timer_base, 1, 100, &timer_100ms);
    //2.1 如果用户串口未在接收状态，并且到时间了，就准备发送
    if (timer_100ms.timer_timeout_flag)
    {
      //2.2 关闭定时器
      timerON(g_timer_base, 0, 100, &timer_100ms);
      //2.3 发送GPS数据给用户
      feedbackGPSDate(g_pos.lat, (float)g_pos.lon, (float)g_pos.relative_alt / 1000.0f);
    }

    //3. 打开1s定时器，发送状态灯
    timerON(g_timer_base, 1, 1000, &timer_1s);
    if (timer_1s.timer_timeout_flag)
    {
      timerON(g_timer_base, 0, 1000, &timer_1s);
      HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
                                | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

uint8_t serial_read_ch(void)
{
  uint8_t ch;
  Queue_Pop(&rx_queue, &ch);
  return ch;
}

uint16_t serial_available(void)
{
  return Queue_Count(&rx_queue);
}

mavlink_message_t msg;
mavlink_status_t status;
void update(void)
{
  status.packet_rx_drop_count = 0;
  while (serial_available())
  {
    uint8_t c = serial_read_ch();
    if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
    {
      switch (msg.msgid)
      {
        case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:                                //拿到gps数据
          mavlink_msg_global_position_int_decode(&msg, &g_pos);
          break;
        case MAVLINK_MSG_ID_COMMAND_ACK:
          mavlink_msg_command_ack_decode(&msg, &g_ack);                         //拿到反馈数据
          break;
        default:
          break;
      }
    }
  }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
  if (huart->Instance == USART1)
  {
    Queue_Push(&rx_queue, rx_buffer);                                           //数据入队
    HAL_UART_Receive_IT(&huart1, (uint8_t*)&rx_buffer, 1);                      //再次开启中断
  }
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
