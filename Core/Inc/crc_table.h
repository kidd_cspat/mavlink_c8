#ifndef CRC_TABLE_H
#define CRC_TABLE_H

#include <stdint.h>

uint16_t CRC16_Table(uint8_t *p, uint8_t counter);

#endif // CRC_TABLE_H
