#ifndef __USER_USER_COMM_H_
#define __USER_USER_COMM_H_

#include "stdint.h"
#include "mavlink.h"

#pragma pack (1)

typedef struct
{
  int32_t lat;     //经度
  int32_t lon;     //纬度
  float alt;       //高度
} gps_struct;

typedef struct
{
  uint8_t header1; // 0x5A
  uint8_t header2; // 0xA5
  uint8_t len;
  union data
  {
    gps_struct data_pack;
    uint8_t data_buff[12];
  } data;
  uint16_t crc16;
} gps_frame_struct;

#pragma pack ()

#define USER_BUFFER_SIZE 256


extern uint8_t control_rx_buf[USER_BUFFER_SIZE];
extern gps_frame_struct g_gps_frame;                                            //收到的用户控制指令
extern uint8_t g_user_receive;                                                  //0 空闲；1 开始接收数据；2 完成数据拷贝

void feedbackGPSDate(int32_t _lat, int32_t _lon, float _alt);  
#endif

