#ifndef __USER_MAV_COMM_H_
#define __USER_MAV_COMM_H_

#include "stdint.h"
#include "mavlink.h"

#define MAVLINK_BUFFER_SIZE 512

void writeGlobalPositionInt(int32_t _lat, int32_t _lon, float _alt);          //发送经纬高给无人机去到指定位置（高精度）
void writeGlobalPositionLong(float _lat, float _lon, float _alt);               //发送经纬高给无人机去到指定位置（低精度）

extern uint8_t g_mav_receive;                                                   //0 空闲；1 数据接收中
extern uint8_t mavlink_rx_buf[MAVLINK_BUFFER_SIZE];

//接收到的变量
extern mavlink_global_position_int_t g_pos;                                     //mavlink收到的gps坐标
extern mavlink_command_ack_t g_ack;                                             //mavlink反馈数据
#endif

