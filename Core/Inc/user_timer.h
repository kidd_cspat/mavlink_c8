/**
  ******************************************************************************
  * @file           user_timer.h
  * @brief          包含 user_timer.c 所有对外的全局变量和功能函数声明
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 长沙万为机器人有限公司.
  * All rights reserved.
  ******************************************************************************
  */
#ifndef __USER_TIMER_H__
#define __USER_TIMER_H__

#include <stdint.h>

volatile extern uint16_t g_timer_base;

typedef struct
{
  uint16_t timer_last_value;                                                    //上次定时器时基
  uint8_t  timer_timeout_flag;                                                  //定时器到时标记
  uint16_t timer_sum_value;                                                     //定期器计数累加值
} UserTimer_t;

typedef struct
{
  uint16_t timer_start;                                                         //计时开始
  uint16_t timer_duration;                                                      //计时时间
} UserTimerSum_t;

void timerON(uint16_t clk,uint8_t timer_switch,uint16_t timer_vlaue,UserTimer_t *p); /**< 软件定时器 */
uint8_t timerSum(uint16_t clk, uint8_t timer_switch, UserTimerSum_t* p);             /**< 软件计数器 */

#endif
